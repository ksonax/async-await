﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwait
{
    public static class Tasks
    {
        /// <summary>
        /// Converts GenerateIntArrayWithRandomValues method to task.
        /// </summary>
        /// <returns>Task</returns>
        public static Task<int[]> Task1()
        {
            var task = Task.Run(() => TaskMethods.GenerateIntArrayWithRandomValues());
            Console.WriteLine("\nArray with random values");
            PrintResult(task.Result);

            return task;
        }

        /// <summary>
        /// Converts MultipyArrayByRandomInt method to task.
        /// </summary>
        /// <param name="array">Array to multiply</param>
        /// <returns>Task</returns>
        public static Task<int[]> Task2(int[] array)
        {
            var task = Task.Run(() => TaskMethods.MultipyArrayByRandomInt(array));
            Console.WriteLine("\nArray Multiplied by random int");
            PrintResult(task.Result);

            return task;
        }

        /// <summary>
        /// Converts SortArrayAscending method to task.
        /// </summary>
        /// <param name="array">Array to sort.</param>
        /// <returns>Task</returns>
        public static Task<int[]> Task3(int[] array)
        {
            var task = Task.Run(() => TaskMethods.SortArrayAscending(array));
            Console.WriteLine("\nArray sorted ascending");
            PrintResult(task.Result);

            return task;
        }

        /// <summary>
        /// Converts IntArrayAverage method to task.
        /// </summary>
        /// <param name="array">Array to find average value.</param>
        /// <returns>Task</returns>
        public static Task<double> Task4(int[] array)
        {
            var task = Task.Run(() => TaskMethods.IntArrayAverage(array));
            Console.WriteLine("\nAverage");
            Console.WriteLine(task.Result);

            return task;
        }

        internal static void PrintResult(int[] array)
        {
            foreach (var item in array)
            {
                Console.Write($"{item} ");
            }
            Console.WriteLine();
        }
    }
}
