﻿using System;
using System.Threading.Tasks;

namespace AsyncAwait
{
    public static class AsyncAwait
    {
        /// <summary>
        /// Calls all <see cref="Tasks"/> using async/await.
        /// </summary>
        static async Task Main()
        {
            var task1 = await Tasks.Task1();
            var task2 = await Tasks.Task2(task1);
            var task3 = await Tasks.Task3(task2);
            var task4 = await Tasks.Task4(task3);
        }
    }
}
