# Async/Await

Solution for TAsync/Await

# Program meets following criteria:

The tasks are built according to the description and execute the required functions.

The code is built as simple async/await code without the ContinueWith() TPL methods.

The program returns an average value of the antecedent array of integers.

